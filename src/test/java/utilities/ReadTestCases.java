package utilities;

import static testScripts.BaseClass.currentDir;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadTestCases {


	int SrNo;
	String URL;
	String Title;
	int RowNo;
	
	public int getSrNo() {
		return SrNo;
	}

	public int getRowNo() {
		return RowNo;
	}

	public void setRowNo(int rowNo) {
		RowNo = rowNo;
	}

	public void setSrNo(int srNo) {
		SrNo = srNo;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	String Status;
	
	public static Map<Integer, ReadTestCases> importTestCases(String fileName,String sheetName) throws IOException
	{
		Map<Integer, ReadTestCases> allCases = new HashedMap<Integer, ReadTestCases>();
	
		int rowNo=1;
		
		XSSFWorkbook excelWbook = ExcelUtility.setExcelWorkbook(fileName);
		XSSFSheet excelWSheet = ExcelUtility.setExcelFileSheet(excelWbook,sheetName);
		
		int noRow = excelWSheet.getPhysicalNumberOfRows();
		
        for(int rows = 1; rows < noRow; rows++)
        {
        	int SrNo=0 ;
        	//XSSFRow row = (XSSFRow) rows.next();
        	try
        	{
        	SrNo = Integer.valueOf(ExcelUtility.getCellData(excelWSheet,rowNo, 0));
        	}catch (Exception e) {
				// TODO: handle exception
			}
        	String URL = ExcelUtility.getCellData(excelWSheet,rowNo, 1);
        	String Title = ExcelUtility.getCellData(excelWSheet,rowNo, 2);
        	String Status = ExcelUtility.getCellData(excelWSheet,rowNo, 3);
        	
        	ReadTestCases cases = new ReadTestCases();
        	cases.setSrNo(SrNo);
        	cases.setURL(URL);
        	cases.setTitle(Title);
        	cases.setStatus(Status);
        	cases.setRowNo(rowNo);
        	
        	rowNo++;
        	
        	allCases.put(SrNo, cases);
        	
        	
        }
        excelWbook.close();
        return allCases;
	}
}
