package testScripts;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import utilities.ExcelUtility;
import utilities.ReadTestCases;

public class BaseClass {

	public static final String testDataExcelFileName = "TestCases.xlsx";
	//Main Directory of the project
    public static final String currentDir = System.getProperty("user.dir");
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		int FromCaseNo =1;
		int ToCaseNo = 3;
		
		Logger logger = Logger.getLogger(BaseClass.class);
		
		Map<Integer, ReadTestCases> allCases = new HashMap<Integer, ReadTestCases>();
		
		//System.setProperty("webdriver.chrome.driver", currentDir + "/src/test/resources/chromedriver.exe");
		System.setProperty("webdriver.gecko.driver", currentDir + "/src/test/resources/geckodriver.exe");
		WebDriver driver;
		
		XSSFWorkbook excelWbook = ExcelUtility.setExcelWorkbook(testDataExcelFileName);
		XSSFSheet excelWSheet = ExcelUtility.setExcelFileSheet(excelWbook,"URLData");
		
		
		allCases = ReadTestCases.importTestCases(testDataExcelFileName, "URLData");
				
		for(int CaseNo = FromCaseNo; CaseNo <= ToCaseNo; CaseNo++)
		{
			
			ReadTestCases testCase = allCases.get(CaseNo);
			
			String URL = testCase.getURL();
			int rowNo = testCase.getRowNo();
			
			logger.info("@@@ Execution started for CaseNo :" + CaseNo + " @@@");
			logger.info("### URL :" + URL + " ###");
			//driver = new ChromeDriver();
			driver  = new FirefoxDriver();
				
			driver.manage().window().maximize();
			driver.get(URL);
			String title = driver.getTitle();
			
			logger.info("### Title :" + title + " ###");
			
			ExcelUtility.setCellData(excelWbook,excelWSheet, title, rowNo, 2);
			ExcelUtility.setCellData(excelWbook,excelWSheet, "PASS", rowNo, 3);;
			
			
			driver.quit();
			
			logger.info("@@@ Execution completed for CaseNo :" + CaseNo + " @@@");
		}
		
		excelWbook.close();
		
	}

}
